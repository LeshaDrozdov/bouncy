// search all slider slides
var allSlidesFive = document.querySelectorAll('.news__slide');

// on click slider buttons
var allNavsFive = document.querySelectorAll('.news__btn');

// show first slide
allSlidesFive[0].style.height = '350px';
allSlidesFive[0].style.opacity = '1';

// next slide
function nextSlideFive() {
    clearTimeout(timerFive);
    for (var i = 0; i < allSlidesFive.length; i++) {
        if (allSlidesFive[i].style.height == "350px") {
            for(var j = 0; j < allNavsFive.length; j++) {
                allNavsFive[j].classList.remove('news__active');
                allNavsFive[i].classList.add('news__active');
            }
            if (i == allSlidesFive.length - 1) {
                allSlidesFive[i].style.height = "0px";
                allSlidesFive[0].style.height = "350px";
                allSlidesFive[i].style.opacity = "0";
                allSlidesFive[0].style.opacity = "1";
                autoSliderFive();
                break;
            }
            allSlidesFive[i].style.height = "0px";
            allSlidesFive[i + 1].style.height = "350px";
            allSlidesFive[i].style.opacity = "0";
            allSlidesFive[i + 1].style.opacity = "1";
            autoSliderFive();
            break;
        }
    }
}

// auto slider
var timerFive = 0;
function autoSliderFive() {
    timerFive = setTimeout(nextSlideFive, 3000);
}
autoSliderFive();


for (var i = 0; i < allNavsFive.length; i++) {
    allNavsFive[i].addEventListener('click', function (event) {
        clearTimeout(timerFive);
        for (var x = 0; x < allNavsFive.length; x++) {
            if (allNavsFive[x] == event.currentTarget) {
                allSlidesFive[x].style.height = "350px";
                allSlidesFive[x].style.opacity = "1";
                autoSliderFive();
            } else {
                allSlidesFive[x].style.height = "0px";
                allSlidesFive[x].style.opacity = "0";
            }
        }
        for(var j = 0; j < allNavsFive.length; j++) {
            allNavsFive[j].classList.remove('news__active');
            event.srcElement.classList.add('news__active');
        }
    });
}

// Buttons
var portfolioPrint = document.querySelector('.nav__link__print');
var portfolioIdentity = document.querySelector('.nav__link__identity');
var portfolioBranding = document.querySelector('.nav__link__branding');
var portfolioWeb = document.querySelector('.nav__link__web');
var portfolioHTML = document.querySelector('.nav__link__html');
var portfolioWordpress = document.querySelector('.nav__link__wordpress');
var portfolioAll = document.querySelector('.nav__link__all');

// Items
var galleryPrint = document.querySelectorAll('.portfolio__catalog__print');
var galleryIdentity = document.querySelectorAll('.portfolio__catalog__identity');
var galleryBranding = document.querySelectorAll('.portfolio__catalog__branding');
var galleryWeb = document.querySelectorAll('.portfolio__catalog__web');
var galleryHTML = document.querySelectorAll('.portfolio__catalog__html');
var galleryWordpress = document.querySelectorAll('.portfolio__catalog__wordpress');
var galleryItem = document.querySelectorAll('.portfolio__catalog__item');

// parent

var galleryParent = document.querySelector('.portfolio__catalogImg')


// functions


function allHide() {
    for (var i = 0; i < galleryItem.length; i++) {
        galleryItem[i].style.height = '0px';
        galleryItem[i].style.width = '0px';
        // galleryItem[i].style.opacity = '0';
        galleryItem[i].style.display = 'none';

    }
}

function print() {
    allHide();
    for (var i = 0; i < galleryPrint.length; i++) {
        galleryPrint[i].style.width = '270px';
        galleryPrint[i].style.height = '361px';
        galleryPrint[i].style.display = '';
    }
    galleryParent.style.height = '365px';
    galleryParent.style.flexDirection = 'row';

}

function identity() {
    allHide();
    for (var i = 0; i < galleryIdentity.length; i++) {
        galleryIdentity[0].style.height = '209px';
        galleryIdentity[1].style.height = '229px';
        galleryIdentity[i].style.width = '270px';
        galleryIdentity[i].style.display = '';
    }
    galleryParent.style.height = '235px';
    galleryParent.style.flexDirection = 'row';
}

function branding() {
    allHide();
    for (var i = 0; i < galleryBranding.length; i++) {
        galleryBranding[0].style.height = '357px';
        galleryBranding[1].style.height = '301px';
        galleryBranding[i].style.width = '270px';
        galleryBranding[i].style.display = '';
    }
    galleryParent.style.height = '360px';
    galleryParent.style.flexDirection = 'row';

}

function web() {
    allHide();
    for (var i = 0; i < galleryWeb.length; i++) {
        galleryWeb[0].style.height = '374px';
        galleryWeb[1].style.height = '255px';
        galleryWeb[i].style.width = '270px';
        galleryWeb[i].style.display = '';
    }
    galleryParent.style.height = '376px';
    galleryParent.style.flexDirection = 'row';

}

function html() {
    allHide();
    for (var i = 0; i < galleryHTML.length; i++) {
        galleryHTML[0].style.height = '361px';
        galleryHTML[1].style.height = '269px';
        galleryHTML[i].style.width = '270px';
        galleryHTML[i].style.display = '';
    }
    galleryParent.style.height = '365px';
    galleryParent.style.flexDirection = 'row';

}

function wordpress() {
    allHide();
    for (var i = 0; i < galleryWordpress.length; i++) {
        galleryWordpress[0].style.height = '344px';
        galleryWordpress[1].style.height = '301px';
        galleryWordpress[i].style.width = '270px';
        galleryWordpress[i].style.display = '';
    }
    galleryParent.style.height = '350px';
    galleryParent.style.flexDirection = 'row';

}

function all() {
    galleryParent.style.height = '1000px';
    for (var i = 0; i < galleryItem.length; i++) {
        galleryItem[i].style.height = '361px';
        galleryItem[i].style.width = '270px';
        galleryItem[2].style.height = '209px';
        galleryItem[3].style.height = '229px';
        galleryItem[4].style.height = '344px';
        galleryItem[5].style.height = '375px';
        galleryItem[6].style.height = '301px';
        galleryItem[7].style.height = '374px';
        galleryItem[8].style.height = '255px';
        galleryItem[10].style.height = '269px';
        galleryItem[11].style.height = '301px';
        galleryItem[i].style.display = '';
    }
    galleryParent.style.flexDirection = 'column';
}



// Events
portfolioPrint.addEventListener('click', print);
portfolioIdentity.addEventListener('click', identity);
portfolioBranding.addEventListener('click', branding);
portfolioWeb.addEventListener('click', web);
portfolioHTML.addEventListener('click', html);
portfolioWordpress.addEventListener('click', wordpress);
portfolioAll.addEventListener('click', all);



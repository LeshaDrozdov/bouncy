// search all slider slides
var allSlidesTheree = document.querySelectorAll('.team__slide');

// on click slider buttons
var allNavsTheree = document.querySelectorAll('.team__btn');

// show first slide
allSlidesTheree[0].style.width = '100%';
allSlidesTheree[0].style.opacity = '1';

// next slide
function nextSlideTheree() {
    clearTimeout(timerTheree);
    for (var i = 0; i < allSlidesTheree.length; i++) {
        if (allSlidesTheree[i].style.width == "100%") {
            for(var j = 0; j < allNavsTheree.length; j++) {
                allNavsTheree[j].classList.remove('testimonials__active');
                allNavsTheree[i].classList.add('testimonials__active');
            }
            if (i == allSlidesTheree.length - 1) {
                allSlidesTheree[i].style.width = "0px";
                allSlidesTheree[0].style.width = "100%";
                allSlidesTheree[i].style.opacity = "0";
                allSlidesTheree[0].style.opacity = "1";
                autoSliderTheree();
                break;
            }
            allSlidesTheree[i].style.width = "0px";
            allSlidesTheree[i + 1].style.width = "100%";
            allSlidesTheree[i].style.opacity = "0";
            allSlidesTheree[i + 1].style.opacity = "1";
            autoSliderTheree();
            break;
        }
    }
}

// auto slider
var timerTheree = 0;
function autoSliderTheree() {
    timerTheree = setTimeout(nextSlideTheree, 3000);
}
autoSliderTheree();


for (var i = 0; i < allNavsTheree.length; i++) {
    allNavsTheree[i].addEventListener('click', function (event) {
        clearTimeout(timerTheree);
        for (var x = 0; x < allNavsTheree.length; x++) {
            if (allNavsTheree[x] == event.currentTarget) {
                allSlidesTheree[x].style.width = "100%";
                allSlidesTheree[x].style.opacity = "1";
                autoSliderTheree();
            } else {
                allSlidesTheree[x].style.width = "0px";
                allSlidesTheree[x].style.opacity = "0";
            }
        }
        for(var j = 0; j < allNavsTheree.length; j++) {
            allNavsTheree[j].classList.remove('testimonials__active');
            event.srcElement.classList.add('testimonials__active');
        }
    });
}